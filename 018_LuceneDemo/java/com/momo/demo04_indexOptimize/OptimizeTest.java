package com.momo.demo04_indexOptimize;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriter.MaxFieldLength;
import org.junit.Test;

import com.momo.utils.LuceneUtils;

/**
 * 索引库整理
 * 
 * @author <a href="mailto:haiyong@ssreader.cn">haiyong</a>
 * @version 2013-6-8
 */
public class OptimizeTest {
	@Test
	public void optimize() throws Exception {
		IndexWriter indexWriter = new IndexWriter(LuceneUtils.directory, LuceneUtils.analyzer, MaxFieldLength.LIMITED);
		indexWriter.optimize();
		indexWriter.close();
	}
}

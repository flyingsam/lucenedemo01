package com.momo.utils;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

/**
 * 工具类：创建索引库存放路径以及实例化分词对象
 * 
 * @author <a href="mailto:haiyong@ssreader.cn">haiyong</a>
 * @version 2013-6-8
 */
public class LuceneUtils {
	public static Directory directory;

	public static Analyzer analyzer;
	static {
		try {
			directory = FSDirectory.open(new File("./indexDir"));
			analyzer = new IKAnalyzer();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}

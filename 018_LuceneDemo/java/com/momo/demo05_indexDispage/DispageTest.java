package com.momo.demo05_indexDispage;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.util.Version;
import org.junit.Test;

import com.momo.javabean.Article;
import com.momo.utils.DocumentUtils;
import com.momo.utils.LuceneUtils;

/**
 * 分页
 */
public class DispageTest {

	// 分页类
	private void testSearchIndex(int firstResult, int maxResult) throws Exception {
		IndexSearcher indexSearcher = new IndexSearcher(LuceneUtils.directory);
		QueryParser queryParser = new MultiFieldQueryParser(Version.LUCENE_30, new String[] { "title", "content" }, LuceneUtils.analyzer);
		Query query = queryParser.parse("2013");
		TopDocs topDocs = indexSearcher.search(query, 50);// 查询50条结果
		int count = topDocs.totalHits;// 总的记录数
		int scoreCount = Math.min(count, firstResult + maxResult);// 截止条数
		ScoreDoc[] scoreDocs = topDocs.scoreDocs;

		// 将搜索出的doc转换为model保存在List中
		List<Article> articleList = new ArrayList<Article>();
		for (int i = firstResult; i < scoreCount; i++) {
			int index = scoreDocs[i].doc;
			Document document = indexSearcher.doc(index);
			Article article = DocumentUtils.document2Article(document);
			articleList.add(article);
		}

		// 输出查询结果
		for (Article article : articleList) {
			System.out.println(article.getId());
			System.out.println(article.getTitle());
			System.out.println(article.getContent());
		}
	}

	@Test
	public void dispage() throws Exception {
		this.testSearchIndex(20, 10);// 从查询出来的索引值中，从第20条开始取后面的10条数据
	}
}
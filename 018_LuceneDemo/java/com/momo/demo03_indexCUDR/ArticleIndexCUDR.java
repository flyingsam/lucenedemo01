package com.momo.demo03_indexCUDR;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriter.MaxFieldLength;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.util.Version;
import org.junit.Test;

import com.momo.javabean.Article;
import com.momo.utils.DocumentUtils;
import com.momo.utils.LuceneUtils;

/**
 * @see 创建索引库，把article对象加入到索引库中
 * @see 查询 、修改 、删除
 */
public class ArticleIndexCUDR {
	/**
	 * 创建索引库
	 * @throws Exception
	 */
	@Test
	public void testCreateIndex() throws Exception {
		IndexWriter indexWriter = new IndexWriter(LuceneUtils.directory, LuceneUtils.analyzer, MaxFieldLength.LIMITED);
		Article article = new Article();
		article.setId(1L);
		article.setTitle("NBA总冠军");
		article.setContent("LBJ和韦德能带领热火在2013赛季拿到NBA总冠军吗");
		Document doc = DocumentUtils.article2Document(article);
		indexWriter.addDocument(doc);
		indexWriter.close();// 每次都要关闭
	}

	/**
	 * 批量创建索引库
	 * @throws Exception
	 */
	@Test
	public void testCreateIndexBatch() throws Exception {
		IndexWriter indexWriter = new IndexWriter(LuceneUtils.directory, LuceneUtils.analyzer, MaxFieldLength.LIMITED);
		for (int i = 1; i <= 25; i++) {
			Article article = new Article();
			article.setId(Long.parseLong("" + i));
			article.setTitle("NBA总冠军");
			article.setContent("LBJ和韦德能带领热火在2013赛季拿到NBA总冠军吗");
			Document doc = DocumentUtils.article2Document(article);
			indexWriter.addDocument(doc);
		}
		indexWriter.close();
	}

	/**
	 * 查找索引
	 * @throws Exception
	 */
	@Test
	public void testSearchIndex() throws Exception {
		IndexSearcher indexSearcher = new IndexSearcher(LuceneUtils.directory);
		QueryParser queryParser = new MultiFieldQueryParser(Version.LUCENE_30, new String[] { "title", "content" }, LuceneUtils.analyzer);
		Query query = queryParser.parse("NBA");
		TopDocs topDocs = indexSearcher.search(query, 25);
		int count = topDocs.totalHits;// 总的抓取命中的记录数
		ScoreDoc[] scoreDocs = topDocs.scoreDocs;
		List<Article> articleList = new ArrayList<Article>();
		// 将查询到的索引添加到articleList中
		for (int i = 0; i < scoreDocs.length; i++) {
			int index = scoreDocs[i].doc;
			Document document = indexSearcher.doc(index);
			Article article = DocumentUtils.document2Article(document);
			articleList.add(article);
		}

		// 输出查询到的内容
		for (Article article : articleList) {
			System.out.println(article.getId());
			System.out.println(article.getTitle());
			System.out.println(article.getContent());
		}
	}

	/**
	 * 删除操作是针对关键词对象进行删除的，封装的关键词在目录库中必须存在才能删除
	 * @throws Exception
	 */
	@Test
	public void deleteIndex() throws Exception {
		IndexWriter indexWriter = new IndexWriter(LuceneUtils.directory, LuceneUtils.analyzer, MaxFieldLength.LIMITED);
		Term term = new Term("title", "总冠军");// Term 关键词对象
		indexWriter.deleteDocuments(term);
		indexWriter.close();
	}

	/**
	 * 修改是先删除后增加
	 * @throws Exception
	 */
	@Test
	public void testUpdateIndex() throws Exception {
		IndexWriter indexWriter = new IndexWriter(LuceneUtils.directory, LuceneUtils.analyzer, MaxFieldLength.LIMITED);
		Term term = new Term("title", "总冠军");
		Article article = new Article();
		article.setId(1L);
		article.setTitle("热火总冠军");
		article.setContent("热火在2013赛季拿到NBA总冠军");
		Document doc = DocumentUtils.article2Document(article);
		indexWriter.updateDocument(term, doc);// 第一个参数：term作用是删除、第二个参数：documet作用是增加
		indexWriter.close();
	}
}

package com.momo.demo12_testUtils;

import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.momo.demo11_utils.KeyWordUtil;

public class TestUtils {
	@Test
	public void test001_getKeyWord() throws Exception {
		String str = "中国2013年六月神州十号飞船升空取得了圆满胜利！！";
		System.out.println(KeyWordUtil.getKeyWord(str));
	}

	@Test
	public void test002_strlen() throws Exception {
		String str = "中国2013年六月神州十号飞船升空取得了圆满胜利！！";
		System.out.println(KeyWordUtil.strlen(str, "UTF-8"));
	}

	@Test
	public void test003_termsFormAnalysis() throws Exception {
		String str = "中国2013年六月神州十号飞船升空取得了圆满胜利！！";
		String keys[] = KeyWordUtil.termsFormAnalysis(new IKAnalyzer(true), str);
		for (String key : keys) {
			System.out.print(key + " | ");
		}
	}
}

package com.momo.demo08_sort;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.util.Version;
import org.junit.Test;

import com.momo.javabean.Article;
import com.momo.utils.DocumentUtils;
import com.momo.utils.LuceneUtils;

/**
 * @see 1、相同的结构，相同的关键词，得分一样
 * @see 2、相同的结构，不同的关键词，得分不一样，一般情况下，中文高于英文
 * @see 3、不同的结构，相同的关键词,关键词的得分越高，排名越靠前.关键词出现的频率越高，得分越高
 * @see 4、lucene的竞价排名
 */
public class SortTest {
	@Test
	public void testSearchIndex() throws Exception {
		IndexSearcher indexSearcher = new IndexSearcher(LuceneUtils.directory);
		QueryParser queryParser = new MultiFieldQueryParser(Version.LUCENE_30, new String[] { "title", "content" }, LuceneUtils.analyzer);
		Query query = queryParser.parse("NBA");
		TopDocs topDocs = indexSearcher.search(query, 28);
		int count = topDocs.totalHits;// 总的记录数
		System.err.println("============================命中总条数：" + count);
		ScoreDoc[] scoreDocs = topDocs.scoreDocs;
		List<Article> articleList = new ArrayList<Article>();
		for (int i = 0; i < scoreDocs.length; i++) {
			int index = scoreDocs[i].doc;
			float score = scoreDocs[i].score;// 相关度得分
			System.err.println("======================当前关键字匹配度：" + score);
			Document document = indexSearcher.doc(index);
			Article article = DocumentUtils.document2Article(document);
			articleList.add(article);
		}

		// 输出查询出来的信息
		for (Article article : articleList) {
			System.out.println(article.getId());
			System.out.println(article.getTitle());
			System.out.println(article.getContent());
		}
	}
}
